# Vue widget

simple web application which renders data defined by the [API endpoint](https://search-api.fie.future.net.uk/widget.php?id=review&model_name=xbox_one_s&area=GB).



## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
